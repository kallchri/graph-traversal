package gr.duth.ee.euclid.datastructures.bipartitecheck;

import gr.james.simplegraph.Graph;

import java.io.Serializable;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class BipartiteCheck implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Graph g;
	private boolean isBipartite;
	private int bigGroupSize;
	private int smallGroupSize;

	public BipartiteCheck(Graph g) {
		if (g == null) {
			throw new NullPointerException();
		}
		if (g.size() < 2) {
			throw new IllegalArgumentException();
		}
		this.g = g;
		this.isBipartite = true;
		this.bigGroupSize = -1;
		this.smallGroupSize = -1;
		bipartiteCheck();
	}

	/**
	 * Perform the computation to figure out if {@link #g} is bipartite and
	 * compute and store the values {@link #isBipartite}, {@link #bigGroupSize}
	 * and {@link #smallGroupSize}.
	 * <p>
	 * The value {@link #isBipartite} must be {@code true} is the graph is
	 * bipartite, otherwise {@code false}.
	 * <p>
	 * The value {@link #bigGroupSize} must be equal to the number of vertices
	 * in the bigger of the two groups of the bipartite graph, or {@code -1} if
	 * the graph is not bipartite.
	 * <p>
	 * The value {@link #smallGroupSize} must be equal to the number of vertices
	 * in the smaller of the two groups of the bipartite graph, or {@code -1} if
	 * the graph is not bipartite.
	 */
	private void bipartiteCheck() {
		// STUDENT CODE
	}

	public boolean isBipartite() {
		return isBipartite;
	}

	public int bigGroupSize() {
		return bigGroupSize;
	}

	public int smallGroupSize() {
		return smallGroupSize;
	}
}
